#include <stream9/sqlite/database.hpp>

#include <boost/test/unit_test.hpp>

#include <sys/stat.h>

#include <stream9/filesystem.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace testing {

namespace sqlite = stream9::sqlite;

namespace fs = stream9::filesystem;

using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(database_)

    BOOST_AUTO_TEST_SUITE(last_insert_rowid_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            fs::temporary_directory dir;
            sqlite::database db { dir.path() / "test.db" };

            db.exec("CREATE TABLE table1 ( id INTEGER PRIMARY KEY )");

            BOOST_TEST(db.last_insert_rowid() == 0);

            db.exec("INSERT INTO table1 VALUES ( 100 )");

            auto const id1 = db.last_insert_rowid();
            BOOST_TEST(id1 == 100);

            db.exec("INSERT INTO table1 VALUES ( 200 )");

            auto const id2 = db.last_insert_rowid();
            BOOST_TEST(id2 == 200);
        }

    BOOST_AUTO_TEST_SUITE_END() // last_insert_rowid_

    BOOST_AUTO_TEST_CASE(is_readonly_)
    {
        try {
            fs::temporary_directory dir;
            auto const& db_path = dir.path() / "test.db";

            sqlite::database db1 { db_path };
            BOOST_TEST(!db1.is_readonly());

            ::chmod(db_path.c_str(), 0444);

            sqlite::database db2 { db_path };
            BOOST_TEST(db2.is_readonly());

            ::chmod(db_path.c_str(), 0666);
        }
        catch (...) {
            stream9::print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
