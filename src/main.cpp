#define BOOST_TEST_MODULE main

#include <stream9/sqlite.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace testing {

namespace sqlite = stream9::sqlite;

namespace fs = stream9::filesystem;

using boost::test_tools::per_element;

using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(sqlite_)

    BOOST_AUTO_TEST_CASE(first_step_)
    {
        try {
            fs::temporary_directory dir;
            sqlite::database db { dir.path() / "test.db" };

            db.exec("DROP TABLE IF EXISTS table1");
            db.exec("CREATE TABLE table1 ( id INTEGER PRIMARY KEY )");
            db.exec("INSERT INTO table1 VALUES ( 100 )");

            auto stmt = db.exec("SELECT id FROM table1");

            auto it = stmt.begin();
            auto end = stmt.end();
            BOOST_TEST((it != end));

            auto const row = *it;
            BOOST_TEST(row.get_int(0) == 100);

            ++it;
            BOOST_TEST((it == end));
        }
        catch (...) {
            stream9::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(blob_)
    {
        fs::temporary_directory dir;
        sqlite::database db { dir.path() / "test.db" };
        std::vector<char> data { 0, 1, 2, 3 };

        db.exec("DROP TABLE IF EXISTS table1");
        db.exec("CREATE TABLE table1 ( data BLOB )");

        auto stmt = db.prepare("INSERT INTO table1 VALUES ( :data )");
        stmt.exec(data);

        stmt = db.exec("SELECT data FROM table1");
        BOOST_TEST(!stmt.done());

        auto it = stmt.begin();

        auto const row = *it;
        auto const result = row.get_blob(0);
        BOOST_TEST(result == data, per_element());

        ++it;
        BOOST_CHECK(stmt.done());
    }

BOOST_AUTO_TEST_SUITE_END() // sqlite_

BOOST_AUTO_TEST_SUITE(transaction_)

    BOOST_AUTO_TEST_CASE(commit_)
    {
        fs::temporary_directory dir;
        sqlite::database db { dir.path() / "test.db" };

        db.exec("CREATE TABLE table1 ( id INTEGER )");

        {
            auto trans = db.begin_transaction();

            db.exec("INSERT INTO table1 VALUES ( 100 )");

            trans.commit();
        }

        auto query = db.exec("SELECT COUNT(*) FROM table1");
        BOOST_TEST(!query.done());

        BOOST_TEST(query.current_row().get_int(0) == 1);
    }

    BOOST_AUTO_TEST_CASE(rollback_)
    {
        fs::temporary_directory dir;
        sqlite::database db { dir.path() / "test.db" };

        db.exec("CREATE TABLE table1 ( id INTEGER )");

        {
            auto trans = db.begin_transaction();

            db.exec("INSERT INTO table1 VALUES ( 100 )");

            trans.rollback();
        }

        auto query = db.exec("SELECT COUNT(*) FROM table1");
        BOOST_TEST(!query.done());

        BOOST_TEST(query.current_row().get_int(0) == 0);
    }

    BOOST_AUTO_TEST_CASE(rollback_on_scope_out_)
    {
        fs::temporary_directory dir;
        sqlite::database db { dir.path() / "test.db" };

        db.exec("CREATE TABLE table1 ( id INTEGER )");

        {
            auto trans = db.begin_transaction();

            db.exec("INSERT INTO table1 VALUES ( 100 )");
        }

        auto query = db.exec("SELECT COUNT(*) FROM table1");
        BOOST_TEST(!query.done());

        BOOST_TEST(query.current_row().get_int(0) == 0);
    }

BOOST_AUTO_TEST_SUITE_END() // transaction_

} // namespace testing
